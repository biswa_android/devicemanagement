package com.hcl.app.devicemanagement.login;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.hcl.app.devicemanagement.R;
import com.hcl.app.devicemanagement.navigation.NavigationScreen;
import com.hcl.app.devicemanagement.util.SharedPref;

public class LoginScreen extends Activity {
    private EditText login_edittext;
    private Button login_button;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_screen);
        login_edittext = (EditText) findViewById(R.id.login_edittext);
        login_button = (Button) findViewById(R.id.login_button);

        login_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (login_edittext.getText() != null) {
                    SharedPref sharedPref = new SharedPref(LoginScreen.this);
                    sharedPref.saveUsernmaeInSharedPref(login_edittext.getText().toString().trim());

                    Intent intent = new Intent(LoginScreen.this, NavigationScreen.class);
                    startActivity(intent);
                } else {
                    Toast.makeText(LoginScreen.this, "Please enter username", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
