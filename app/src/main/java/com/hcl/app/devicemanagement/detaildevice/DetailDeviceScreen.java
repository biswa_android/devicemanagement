package com.hcl.app.devicemanagement.detaildevice;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.hcl.app.devicemanagement.R;
import com.hcl.app.devicemanagement.util.DeviceAdapter;
import com.hcl.app.devicemanagement.util.DeviceModel;

import java.util.ArrayList;


public class DetailDeviceScreen extends Activity implements DeviceAdapter.ReturnDevice {
    private ListView detail_listview;
    private ArrayList<DeviceModel> mDetailList;
    private DatabaseReference mDatabaseReference;
    private DeviceModel mDetailDeviceModel;
    private ProgressBar detail_progress_bar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.details_screen);
        detail_listview = (ListView) findViewById(R.id.detail_listview);
        detail_progress_bar = (ProgressBar) findViewById(R.id.detail_progress_bar);
        detail_progress_bar.setVisibility(View.VISIBLE);

        mDetailList = new ArrayList<>();
        mDetailDeviceModel = new DeviceModel();

        mDatabaseReference = FirebaseDatabase.getInstance().getReference("DeviceDetails");
        mDatabaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                detail_progress_bar.setVisibility(View.GONE);
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    mDetailDeviceModel = snapshot.getValue(DeviceModel.class);
                    mDetailList.add(mDetailDeviceModel);
                    detail_listview.setAdapter(
                            new DeviceAdapter(DetailDeviceScreen.this, mDetailList,
                                    false, DetailDeviceScreen.this));
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    @Override
    public void onReturnClicked(String deviceName) {
        //do nothing
    }
}
