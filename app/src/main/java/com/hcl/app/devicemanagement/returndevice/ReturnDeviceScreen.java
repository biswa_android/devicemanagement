package com.hcl.app.devicemanagement.returndevice;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.hcl.app.devicemanagement.R;
import com.hcl.app.devicemanagement.util.DeviceAdapter;
import com.hcl.app.devicemanagement.util.DeviceModel;
import com.hcl.app.devicemanagement.util.SharedPref;

import java.util.ArrayList;

public class ReturnDeviceScreen extends Activity implements DeviceAdapter.ReturnDevice {
    private ListView return_listview;
    private ArrayList<DeviceModel> mReturnList;
    private DeviceModel mDeviceModel;
    private DatabaseReference mDatabaseReference;
    private SharedPref mSharedPref;
    private ProgressBar return_progress_bar;
    private DeviceAdapter mDeviceAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.return_screen);
        return_listview = (ListView) findViewById(R.id.return_listview);
        return_progress_bar = (ProgressBar) findViewById(R.id.return_progress_bar);
        return_progress_bar.setVisibility(View.VISIBLE);

        mReturnList = new ArrayList<>();
        mDeviceModel = new DeviceModel();
        mSharedPref = new SharedPref(ReturnDeviceScreen.this);
        mDatabaseReference = FirebaseDatabase.getInstance().getReference("DeviceDetails");
        Query query = mDatabaseReference.orderByChild("currentUser").equalTo(mSharedPref.getUsernameFromSharedPref());
        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                return_progress_bar.setVisibility(View.GONE);
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    mDeviceModel = snapshot.getValue(DeviceModel.class);
                    mReturnList.add(mDeviceModel);
                    mDeviceAdapter = new DeviceAdapter(ReturnDeviceScreen.this, mReturnList,
                            true, ReturnDeviceScreen.this);
                    mDeviceAdapter.notifyDataSetChanged();
                    return_listview.setAdapter(mDeviceAdapter);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    @Override
    public void onReturnClicked(String deviceName) {
        mDatabaseReference = FirebaseDatabase.getInstance().getReference("DeviceDetails").child(deviceName);
        mDatabaseReference.removeValue();
        Toast.makeText(ReturnDeviceScreen.this, "Device Returned", Toast.LENGTH_SHORT).show();
    }
}
