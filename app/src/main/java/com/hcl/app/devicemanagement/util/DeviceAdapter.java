package com.hcl.app.devicemanagement.util;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.hcl.app.devicemanagement.R;

import java.util.ArrayList;

public class DeviceAdapter extends BaseAdapter {
    private ArrayList<DeviceModel> mDeviceModels;
    private Context mContext;
    private boolean mReturn;
    private ReturnDevice mReturnDevice;

    public DeviceAdapter(Context context, ArrayList<DeviceModel> deviceModels, boolean returnDevice, ReturnDevice listener) {
        this.mContext = context;
        this.mDeviceModels = deviceModels;
        this.mReturn = returnDevice;
        mReturnDevice = listener;
    }

    @Override
    public int getCount() {
        return mDeviceModels.size();
    }

    @Override
    public Object getItem(int position) {
        return mDeviceModels.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.details_device_adapter, null);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        if (mReturn) {
            holder.adapter_return.setVisibility(View.VISIBLE);
        }
        holder.adapter_device_name.setText("DEVICE NAME : " + mDeviceModels.get(position).getDeviceName());
        holder.adapter_accessories.setText("ACCESSORIES TAKEN : " + mDeviceModels.get(position).getAccessories());
        holder.adapter_current_user.setText("CURRENT USER : " + mDeviceModels.get(position).getCurrentUser());
        holder.adapter_date.setText("TAKEN WHEN : " + mDeviceModels.get(position).getStartDate());
        holder.adapter_return.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mReturnDevice.onReturnClicked(mDeviceModels.get(position).getDeviceName());
            }
        });
        return convertView;
    }

    static class ViewHolder {
        TextView adapter_device_name, adapter_accessories, adapter_current_user, adapter_date;
        Button adapter_return;

        public ViewHolder(View view) {
            adapter_device_name = (TextView) view.findViewById(R.id.adapter_device_name);
            adapter_accessories = (TextView) view.findViewById(R.id.adapter_accessories);
            adapter_current_user = (TextView) view.findViewById(R.id.adapter_current_user);
            adapter_date = (TextView) view.findViewById(R.id.adapter_date);
            adapter_return = (Button) view.findViewById(R.id.adapter_return);
        }
    }

    public interface ReturnDevice {
        void onReturnClicked(String deviceName);
    }
}
