package com.hcl.app.devicemanagement.takedevice;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.hcl.app.devicemanagement.R;
import com.hcl.app.devicemanagement.util.DeviceModel;
import com.hcl.app.devicemanagement.util.SharedPref;

import java.text.SimpleDateFormat;
import java.util.Date;

public class TakeDeviceScreen extends Activity {
    private Spinner home_spinner;
    private CheckBox home_accessories;
    private Button home_save;
    private DeviceModel mDeviceModel;
    private DatabaseReference mDatabaseReference;
    private SharedPref mSharedPref;
    SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
    Date date = new Date();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home_screen);

        mDatabaseReference = FirebaseDatabase.getInstance().getReference("DeviceDetails");
        mDeviceModel = new DeviceModel();
        mSharedPref = new SharedPref(TakeDeviceScreen.this);

        home_spinner = (Spinner) findViewById(R.id.home_spinner);
        home_save = (Button) findViewById(R.id.home_save);
        home_accessories = (CheckBox) findViewById(R.id.home_accessories);

        home_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDatabaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        if (dataSnapshot.hasChild(home_spinner.getSelectedItem().toString().trim())) {
                            Toast.makeText(TakeDeviceScreen.this, "Device not available", Toast.LENGTH_SHORT).show();
                        } else {
                            insertDeviceDetail();
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });

            }
        });
    }

    private void insertDeviceDetail() {
        mDatabaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                getValues();
                mDatabaseReference.child(home_spinner.getSelectedItem().toString().trim()).setValue(mDeviceModel);
                Toast.makeText(TakeDeviceScreen.this, "Submitted Successfully", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                //do nothing
            }
        });
    }

    private void getValues() {
        mDeviceModel.setDeviceName(home_spinner.getSelectedItem().toString().trim());
        mDeviceModel.setCurrentUser(mSharedPref.getUsernameFromSharedPref());
        mDeviceModel.setStartDate(formatter.format(date));
        if (home_accessories.isChecked()) {
            mDeviceModel.setAccessories("Yes");
        } else {
            mDeviceModel.setAccessories("No");
        }

    }
}
