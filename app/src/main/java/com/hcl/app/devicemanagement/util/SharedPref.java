package com.hcl.app.devicemanagement.util;

import android.content.Context;
import android.content.SharedPreferences;

public class SharedPref {
    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;

    public SharedPref(Context context) {
        preferences = context.getSharedPreferences("devManage", 0);
        editor = preferences.edit();
    }

    public void saveUsernmaeInSharedPref(String value) {
        editor.putString("username", value);
        editor.apply();
    }

    public String getUsernameFromSharedPref() {
        return preferences.getString("username", null);
    }
}
