package com.hcl.app.devicemanagement.navigation;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.annotation.Nullable;

import com.hcl.app.devicemanagement.R;
import com.hcl.app.devicemanagement.detaildevice.DetailDeviceScreen;
import com.hcl.app.devicemanagement.returndevice.ReturnDeviceScreen;
import com.hcl.app.devicemanagement.takedevice.TakeDeviceScreen;

public class NavigationScreen extends Activity implements View.OnClickListener {
    private Button nav_take_device, nav_return_device, nav_details_device;
    private Intent mIntent;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.navigation_screen);
        nav_take_device = (Button) findViewById(R.id.nav_take_device);
        nav_return_device = (Button) findViewById(R.id.nav_return_device);
        nav_details_device = (Button) findViewById(R.id.nav_details_device);
        nav_take_device.setOnClickListener(this);
        nav_return_device.setOnClickListener(this);
        nav_details_device.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.nav_take_device:
                mIntent = new Intent(NavigationScreen.this, TakeDeviceScreen.class);
                startActivity(mIntent);
                break;
            case R.id.nav_return_device:
                mIntent = new Intent(NavigationScreen.this, ReturnDeviceScreen.class);
                startActivity(mIntent);
                break;
            case R.id.nav_details_device:
                mIntent = new Intent(NavigationScreen.this, DetailDeviceScreen.class);
                startActivity(mIntent);
                break;
            default:
                break;
        }
    }
}
